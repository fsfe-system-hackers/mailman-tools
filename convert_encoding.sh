#!/bin/bash
# SPDX-FileCopyrightText: 2019 Max Mehl <max.mehl@fsfe.org>
# SPDX-License-Identifier: GPL-3.0-or-later

# Invoke with something like:
# lang="en de es"
# for l in $lang; do
#   ~/mailman-tools/convert_encoding.sh find $l/LC_MESSAGES/mailman.po
# done

MODE=$1
FILE=$2

filename=$(basename -- "${FILE}")
dirname=$(dirname -- "${FILE}")
extension="${filename##*.}"
filename="${filename%.*}"

if `file -bi "${FILE}" | grep -q "charset=iso-8859-1"`; then
  if [[ "$MODE" == "find" ]]; then
    echo "${FILE} is in wrong format"
  elif [[ "$MODE" == "change" ]]; then
    echo "Converting ${FILE}"
    cp "${FILE}" "${FILE}.bak"
    recode iso-8859-1..utf8 "${FILE}"
    sed -i "s/ISO-8859-1/UTF-8/gi" "${FILE}"
    /var/lib/mailman/bin/msgfmt.py -o "${dirname}/${filename}.mo" "${FILE}"
  else
    echo "Either use \"find\" or \"change\" as second argument."
  fi
fi
