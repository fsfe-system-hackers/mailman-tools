#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Michal Gorlas <micgor32@fsfe.org>
# SPDX-License-Identifier: GPL-3.0-or-later


import os
import subprocess
import sys

from mailman.testing.documentation import cli

# DISCLAIMER: This script is assumes that Mailman3 is installed, and archives lists contains both *.mbox and *.pck for each mailing list that you want to migrate
# Usage: python3 migration2to3_import.py <pythonpath> <settings> <archives_dir> <lists_address>
#
# pythonpath - location of your Hyperkitty instalattion (on default debian installation /usr/share/mailman3-web)
# settings - "settings" or "settings_local" ("settings_local" is recommended in the Hyperkitty documentation: https://docs.mailman3.org/projects/hyperkitty/en/latest/install.html#setup-your-django-project)
# archives_dir - directory with .mbox and .pck files (user extract.py on mailman2 directories)
# lists_address - address that comes after list name, eg. lists.fsfe.org


def usage():
    print(
        """
Usage: python3 migration2to3_import.py <pythonpath> <settings> <archives_dir> <lists_domain>
 
 pythonpath - location of your Hyperkitty instalattion (on default debian installation /usr/share/mailman3-web)
 settings - "settings" or "settings_local" ("settings_local" is recommended in the Hyperkitty documentation: https://docs.mailman3.org/projects/hyperkitty/en/latest/install.html#setup-your-django-project)
 archives_dir - directory with .mbox files and .pck files
 lists_domain - domain for the list, eg. lists.fsfe.org
    """
    )


def import_lists(archives_dir, lists_domain):
    # Creating list and importing the config for given list
    for lst in os.listdir(archives_dir):
        if lst.endswith(".pck"):
            list_config_path = os.path.join(archives_dir, lst)
            list_name = lst.split(".pck")[0] + "@" + lists_domain

            create_list(list_name, list_config_path)


def create_list(list_name, pck_path):
    # Likely have to be adjusted with "--run-as-root" due to specifics of our config on list2
    # Idea: add arg to this function that takes True or False and based on that chooses whetever to
    # run as root or not.
    create_cmd = f"mailman --run-as-root create {list_name}"
    import_cmd = f"mailman --run-as-root import21 {list_name} {pck_path}"
    subprocess.run(create_cmd, shell=True)
    subprocess.run(import_cmd, shell=True)


def import_archives(pythonpath, settings, archives_dir, lists_domain):
    for mbox in os.listdir(archives_dir):
        if mbox.endswith(".mbox"):
            archive_path = os.path.join(archives_dir, mbox)
            list_name = mbox.split(".mbox")[0] + "@" + lists_domain

            import_cmd = f"django-admin hyperkitty_import --pythonpath {pythonpath} --settings {settings} -l {list_name} {archive_path}"
            subprocess.run(import_cmd, shell=True)
        else:
            pass

    refresh_indexes = (
        f"django-admin update_index --pythonpath {pythonpath} --settings {settings}"
    )
    subprocess.run(refresh_indexes, shell=True)


if __name__ == "__main__":
    if len(sys.argv) < 5:
        usage()
    else:
        import_lists(sys.argv[3], sys.argv[4])
        import_archives(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
