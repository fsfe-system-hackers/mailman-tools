#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Michal Gorlas <micgor32@fsfe.org>
# SPDX-License-Identifier: GPL-3.0-or-later


import os
import shutil
import subprocess
import sys


def usage():
    print(
        """
Usage: python3 migration2to3_import.py <config_path> <archives_path> <output_dir>

  config_path - directory with .pck files (on Debian /var/lib/mailman/lists by default)
  archives_path - directory with .mbox files (on Debian /var/lib/mailman/archives/private by default)
  output_dir - path for directory where files are going to be stored after this script finishes its work
        """
    )


def extract_config(config_path, output_dir):
    for sub_dir in os.listdir(config_path):
        path_w_subdir = os.path.join(config_path, sub_dir)
        if os.path.isdir(path_w_subdir):
            for conf in os.listdir(path_w_subdir):
                out_name = os.path.basename(os.path.normpath(path_w_subdir))
                if conf == "config.pck":
                    shutil.copy2(os.path.join(path_w_subdir, conf), str(os.path.join(output_dir, out_name) + ".pck"))


def extract_archives(archives_path, output_dir):
    for sub_dir in os.listdir(archives_path):
        path_w_subdir = os.path.join(archives_path, sub_dir)
        if os.path.isdir(path_w_subdir) and path_w_subdir.endswith(".mbox"):
            for arc in os.listdir(path_w_subdir):
                if arc.endswith(".mbox"):
                    shutil.copy2(os.path.join(path_w_subdir, arc), output_dir)


if __name__ == "__main__":
    if len(sys.argv) < 4:
        usage()
    else:
        extract_config(sys.argv[1], sys.argv[3])
        extract_archives(sys.argv[2], sys.argv[3])
