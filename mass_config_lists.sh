#!/bin/bash
# SPDX-FileCopyrightText: 2019 Max Mehl <max.mehl@fsfe.org>
# SPDX-License-Identifier: GPL-3.0-or-later

# Changes one or multiple configs for all mailing lists on this server.

PROGNAME="$(basename "$0" .sh)"

print_help() {
  echo "Usage:    $PROGNAME [-i <config_file> | -o <config_variable>] [-l <lists>"
  echo "Example:  $PROGNAME -i mass_config_lists.cfg"
  echo "Example:  $PROGNAME -o private_roster -l press-release,android,france"
}

while getopts i:o:l:h OPT; do
  case $OPT in
    i)  INPUT=$OPTARG;;
    o)  OUTPUT=$OPTARG;;
    l)  LISTS=$OPTARG;;
    h)  print_help; exit 0;;
    *)  echo "UNKNOWN: Unknown option: -$OPTARG"; print_help; exit 1;;
  esac
done

# Sanity checks
if [[ -n "${INPUT}" && -n "${OUTPUT}" ]]; then
  echo "Only use either -o or -i, not both together."
  print_help
  exit 1
fi

# Define lists
ALL_LISTS="$(list_lists --bare)"
if [[ -n "${LISTS}" ]]; then
  # space-separate lists for "for" clause
  LISTS="$(echo ${LISTS} | sed -e 's/,/ /g')"

  # check if all given lists exist
  for list in $LISTS; do
    if ! echo "${ALL_LISTS}" | grep -Eq "^${list}$"; then
      echo "$list does not exist"
      exit 1
    fi
  done
else
  LISTS="${ALL_LISTS}"
fi

# Update list configs
if [[ -n "${INPUT}" ]]; then
  if [[ ! -e "${INPUT}" ]]; then
    echo "File ${INPUT} does not exist!"
    exit 1
  fi
  for list in $LISTS; do
    config_list -i "${INPUT}" $list
    echo "$list updated."
  done
# Output list configs
elif [[ -n "${OUTPUT}" ]]; then
  for list in $LISTS; do
    echo "$list: $(config_list -o- $list | grep -i "${OUTPUT}")"
  done | column -t -s":"
# Catch no input
else
  echo "Provide either -i or -o with a file or variable"
  print_help
  exit 1
fi
