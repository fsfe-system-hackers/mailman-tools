# Mailman Tools

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/mailman-tools/00_README)

Some extra tools for the FSFE Mailman server.
